"use strict";

const loadBtnW = document.querySelector(".btn-import-work .btn");
let countClicks = 0,
  dataBoxFltr = document.querySelectorAll(".filters-box"),
  fadeBoxFltr = document.querySelector(".download");

fadeBoxFltr.style.display = "none";
// ----------- "Services Ham" Section ---------------
document.querySelector(".svcs-tab").addEventListener("click", (e) => {
  document.querySelector(".tab-item.focus").classList.remove("focus");
  e.target.classList.add("focus");

  document.querySelector('[data-display="on"]').classList.add("display-off");
  document.querySelector('[data-display="on"]').dataset.display = "off";

  if (
    e.target.dataset.tab ==
    document.querySelector(
      '.svcs-text[data-text="' + e.target.dataset.tab + '"]'
    ).dataset.text
  ) {
    document
      .querySelector('.svcs-text[data-text="' + e.target.dataset.tab + '"]')
      .classList.remove("display-off");
    document.querySelector(
      '.svcs-text[data-text="' + e.target.dataset.tab + '"]'
    ).dataset.display = "on";
  }
});

//============= Filters Img ===========
function showImg(x) {
  for (let i = 0; i < dataBoxFltr.length; i++) {
    if (dataBoxFltr[i].dataset.filter == x.dataset.filter) {
      $(dataBoxFltr[i]).fadeIn("slow");
    } else if (x.dataset.filter == "all") {
      $(dataBoxFltr[i]).fadeIn("slow");
    } else {
      dataBoxFltr[i].style.display = "none";
    }
  }
}

//-------------func loader(CSS)-------------
function animatedLoad() {
  $(".inner").slideUp("slow");
  $(".lds-ellipsis").slideDown("slow");
  setTimeout(() => {
    $(".lds-ellipsis").slideUp();
    $(".inner").fadeIn("slow");
  }, 2000);
}

// ------------------ Btn Loader -------------
loadBtnW.addEventListener("click", () => {
  if (countClicks < 2) {
    animatedLoad();
    setTimeout(() => {
      fadeBoxFltr.style.display = "flex";
    }, 2100);
    countClicks++;
  } else return;
});

//-------------- "Filters of Work" section ---------------
$(".filters-item").click((e) => {
  //adding&remove focus from old btn&img
  document
    .querySelector(".filters-item-focus")
    .classList.remove("filters-item-focus");
  e.target.classList.add("filters-item-focus");

  let dataBtnSlctd = document.querySelector(
    '.filters-item[data-filter="' + e.target.dataset.filter + '"]'
  );

  showImg(dataBtnSlctd);
});

//----------- Feads section - w/ slick -----------------
$(document).ready(function () {
  $(".feadbacks-slider-box").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: ".dots-user",
  });
  $(".dots-user").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".feadbacks-slider-box",
    arrows: true,
    focusOnSelect: true,
    centerMode: true,
  });
});

// -----------BTN UP----------
$("#arrow-up>div").css("display", "none");
$(document).scroll(function () {
  var y = $(this).scrollTop();
  if (y >= 1000) {
    $("#arrow-up>div").fadeIn();
  } else {
    $("#arrow-up>div").fadeOut();
  }
});
